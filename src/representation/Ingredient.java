package representation;

import jcolibri.datatypes.Instance;
import jcolibri.exception.OntologyAccessException;

public class Ingredient {
	private double quantity;
	private String unit;
	private Instance food;
	
	public Ingredient(double quantity, String unit, Instance food) {
		super();
		this.quantity = quantity;
		this.unit = unit;
		this.food = food;
	}
	
	public Ingredient(double quantity, String unit, String food){
		super();
		this.quantity = quantity;
		this.unit = unit;
		try {
			this.food = new Instance(food);
		} catch (OntologyAccessException e) {
			System.err.println("Unable to create ingredient: " + quantity + " " + unit + " " + food);
			e.printStackTrace();
		}
	}

	public double getQuantity() {
		return quantity;
	}
	
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Instance getFood() {
		return food;
	}

	public void setFood(Instance food) {
		this.food = food;
	}

	@Override
	public String toString() {
		return quantity + " " + unit + " " + food;
	}
}
