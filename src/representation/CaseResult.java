package representation;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

public class CaseResult implements CaseComponent{

	private String result;

	@Override
	public Attribute getIdAttribute() {
		return new Attribute("result", this.getClass());
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	

	@Override
	public String toString() {
		return "MyResult: " + result;
	}


}
