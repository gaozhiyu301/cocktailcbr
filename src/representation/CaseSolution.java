package representation;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;


public class CaseSolution implements CaseComponent{

	private String solutionId;
	private Boolean solution;
	

	@Override
	public Attribute getIdAttribute() {
		return new Attribute("solutionId", this.getClass());
	}

	@Override
	public String toString() {
		return "MySolution: " + solutionId + " | " + solution;
	}

	public String getSolutionId() {
		return solutionId;
	}

	public void setSolutionId(String solutionId) {
		this.solutionId = solutionId;
	}

	public Boolean getSolution() {
		return solution;
	}

	public void setSolution(Boolean solution) {
		this.solution = solution;
	}	
	

}
