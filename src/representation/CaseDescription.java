package representation;

import java.util.ArrayList;
import java.util.Collection;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;
import jcolibri.datatypes.Instance;
import objType.Alcohol;
import objType.AlcoholicEnum;
import objType.Enhancer;
import objType.Fruit;
import objType.Garnish;
import objType.Sweetener;


public class CaseDescription implements CaseComponent {

	/*public enum Month{January, February, March, April, May, Jun,
						Jul, Aug, Sep, Oct, Nov, Dec}*/
	
	private String caseId;
	private String title;
	private String steps;
	private AlcoholicEnum isAlcholic;
	private Alcohol alcohols;
	private Enhancer enhancers;
	private Garnish garnishes;
	private Sweetener sweetner;
	private Fruit fruiter;
	public AlcoholicEnum getIsAlcholic() {
		return isAlcholic;
	}

	public void setIsAlcholic(AlcoholicEnum isAlcholic) {
		this.isAlcholic = isAlcholic;
	}

	public Alcohol getAlcohols() {
		return alcohols;
	}

	public void setAlcohols(Alcohol alcohols) {
		this.alcohols = alcohols;
	}

	public Enhancer getEnhancers() {
		return enhancers;
	}

	public void setEnhancers(Enhancer enhancers) {
		this.enhancers = enhancers;
	}

	public Garnish getGarnishes() {
		return garnishes;
	}

	public void setGarnishes(Garnish garnishes) {
		this.garnishes = garnishes;
	}

	public Sweetener getSweetner() {
		return sweetner;
	}

	public void setSweetner(Sweetener sweetner) {
		this.sweetner = sweetner;
	}

	public Fruit getFruiter() {
		return fruiter;
	}

	public void setFruiter(Fruit fruiter) {
		this.fruiter = fruiter;
	}

	public Collection<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Collection<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	private Collection<Ingredient> ingredients = new ArrayList<Ingredient>();
	private Instance test;	//TESTING
	
	
	
	public Instance getTest() {
		return test;
	}

	public void setTest(Instance test) {
		this.test = test;
	}


	
	@Override
	public String toString() {
		return "CaseDescription [caseId=" + caseId + ", title=" + title
				+ ", steps=" + steps + ", isAlcholic=" + isAlcholic
				+ ", alcohols=" + alcohols + ", enhancers=" + enhancers
				+ ", garnishes=" + garnishes + ", sweetner=" + sweetner
				+ ", fruiter=" + fruiter + ", ingredients=" + ingredients
				+ ", test=" + test + "]";
	}

	public void addIngredient(Ingredient ingredient){
		ingredients.add(ingredient);
		test = ingredient.getFood();	// TESTING
	}
	
	
	
	
	public String getTitle() {
		return title;
	}






	public void setTitle(String title) {
		this.title = title;
	}






	public String getSteps() {
		return steps;
	}






	public void setSteps(String steps) {
		this.steps = steps;
	}






	@Override
	public Attribute getIdAttribute() {
		return new Attribute("caseId", this.getClass());
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	
	
	

	
	
	
	
	
}