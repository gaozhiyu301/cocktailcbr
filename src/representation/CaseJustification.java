package representation;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

public class CaseJustification implements CaseComponent{
	private String justification;

	@Override
	public Attribute getIdAttribute() {
		return new Attribute("justification", this.getClass());
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

	@Override
	public String toString() {
		return "MyJustification: " + justification;
	}
	
}
