package objType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Enhancer {
	private static Enhancer instance = null;
	private ArrayList<String> enhancers = new ArrayList<String>();

	private Enhancer(){}
	
	public static Enhancer getInstance(){
		if (instance== null){
			instance = new Enhancer();
		}
		return instance;
	}
	
	public Collection<String> getEnhancers() {
		Collections.sort(enhancers);
		return enhancers;
	}
	
	public void addEnhancer(String enhancer){
		enhancers.add(enhancer);
	}
	
	public void printAllEnhancers(){
		System.out.println("Enhancers:");
		for (String enhancer : enhancers){
			System.out.println("\t" + enhancer);
		}
	}

	@Override
	public String toString() {
		return "Enhancer [enhancers=" + enhancers + "]";
	}
	
	

}
