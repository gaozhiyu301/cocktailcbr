package objType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Fruit {
	private static Fruit instance = null;
	private ArrayList<String> fruits = new ArrayList<String>();

	private Fruit(){}
	
	public static Fruit getInstance(){
		if (instance== null){
			instance = new Fruit();
		}
		return instance;
	}
	
	public Collection<String> getFruits() {
		Collections.sort(fruits);
		return fruits;
	}
	
	public void addFruit(String fruit){
		fruits.add(fruit);
	}
	
	public void printAllFruits(){
		System.out.println("Fruits:");
		for (String fruit : fruits){
			System.out.println("\t" + fruit);
		}
	}

	@Override
	public String toString() {
		return "Fruit [fruits=" + fruits + "]";
	}
	
	
	
	
}
