package objType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Sweetener {
	private static Sweetener instance = null;
	private ArrayList<String> sweeteners = new ArrayList<String>();

	private Sweetener(){}
	
	public static Sweetener getInstance(){
		if (instance== null){
			instance = new Sweetener();
		}
		return instance;
	}
	
	public Collection<String> getSweetener() {
		Collections.sort(sweeteners);
		return sweeteners;
	}
	
	public void addSweetener(String sweetener){
		sweeteners.add(sweetener);
	}
	
	public void printAllSweeteners(){
		System.out.println("Sweeteners:");
		for (String sweetener : sweeteners){
			System.out.println("\t" + sweetener);
		}
	}

	@Override
	public String toString() {
		return "Sweetener [sweeteners=" + sweeteners + "]";
	}
	
	
}
