package objType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Garnish {
	private static Garnish instance = null;
	private ArrayList<String> garnishes = new ArrayList<String>();

	private Garnish(){}
	
	public static Garnish getInstance(){
		if (instance== null){
			instance = new Garnish();
		}
		return instance;
	}
	
	public Collection<String> getGarnishes() {
		Collections.sort(garnishes);
		return garnishes;
	}
	
	public void addGarnish(String garnish){
		garnishes.add(garnish);
	}
	
	public void printAllGarnishes(){
		System.out.println("Garnishes:");
		for (String garnish : garnishes){
			System.out.println("\t" + garnish);
		}
	}

	@Override
	public String toString() {
		return "Garnish [garnishes=" + garnishes + "]";
	}
	
	
}
