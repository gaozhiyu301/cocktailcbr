package objType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Alcohol {
	private static Alcohol instance = null;
	private ArrayList<String> alcohols = new ArrayList<String>();

	private Alcohol(){}
	
	public static Alcohol getInstance(){
		if (instance== null){
			instance = new Alcohol();
		}
		return instance;
	}
	
	public Collection<String> getAlcohols() {
		Collections.sort(alcohols);
		return alcohols;
	}
	
	public void addAlcohol(String alcohol){
		alcohols.add(alcohol);
	}
	
	public void printAllAlcohols(){
		System.out.println("Alcohols:");
		for (String alcohol : alcohols){
			System.out.println("\t" + alcohol);
		}
	}

	@Override
	public String toString() {
		return "Alcohol [alcohols=" + alcohols + "]";
	}
	
	
}
