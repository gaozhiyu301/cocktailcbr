package mycbrcore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.method.retrieve.RetrievalResult;
import jcolibri.method.retrieve.NNretrieval.NNConfig;
import jcolibri.method.retrieve.NNretrieval.NNScoringMethod;
import jcolibri.method.retrieve.NNretrieval.similarity.GlobalSimilarityFunction;
import jcolibri.util.ProgressController;

public class NNScoringMethodOverride {

	
		public static Collection<RetrievalResult> evaluateSimilarity(
				Collection<CBRCase> cases, CBRQuery query, NNConfig simConfig) {
			List res = new ArrayList();
			ProgressController.init(NNScoringMethodOverride.class,
					"Numeric Similarity Computation", cases.size());
			GlobalSimilarityFunction gsf = simConfig.getDescriptionSimFunction();
			for (CBRCase _case : cases) {
				res.add(new RetrievalResult(_case, Double.valueOf(gsf.compute(
						_case.getDescription(), query.getDescription(), _case,
						query, simConfig))));
				ProgressController.step(NNScoringMethodOverride.class);
			}
			Collections.sort(res);
			ProgressController.finish(NNScoringMethodOverride.class);

			return res;
		}
	}