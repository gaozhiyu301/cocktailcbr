package mycbrcore;

import java.util.ArrayList;

import jcolibri.util.FileIO;
import objType.Alcohol;
import objType.Enhancer;
import objType.Fruit;
import objType.Garnish;
import objType.Sweetener;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import es.ucm.fdi.gaia.ontobridge.OntoBridge;
import es.ucm.fdi.gaia.ontobridge.OntologyDocument;

/**
 * <b>Purpose 1:</b> Setting up of ontologies <br>
 * <b>Purpose 2:</b> Extracting objects into their various categories:
 * <ul>
 *    <li>Alcohol</li>
 *    <li>Enhancer</li>
 *    <li>Fruit</li>
 *    <li>Garnish</li>
 *    <li>Sweetener</li>
 * </ul>
 */
public class MyOntology {


	public MyOntology(){
		// Ontology
		OntoBridge ob = jcolibri.util.OntoBridgeSingleton.getOntoBridge();
		ob.initWithPelletReasoner();	// Configure it to work with the Pellet reasoner
		OntologyDocument mainOnto = new OntologyDocument("http://www.semanticweb.org/remote/ontologies/2017/5/untitled-ontology-5", 
				FileIO.findFile("db/main_ontology.owl").toExternalForm());
		ArrayList<OntologyDocument> subOntologies = new ArrayList<OntologyDocument>();	// No subontologies
		ob.loadOntology(mainOnto, subOntologies, false);	// Load the ontology


		//-----------------------------------------------------------
		//System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		//String thingUrl = ob.getThingURI();

		OntModel model = ob.getModel();

		ExtendedIterator iteratorCategory = model.listHierarchyRootClasses();
		while (iteratorCategory.hasNext()){
			Object obj = iteratorCategory.next();		
			Resource resource = (Resource)obj;
			String categoryName = resource.getLocalName();
			//System.out.println("Category name: " + categoryName);

			ExtendedIterator iteratorFood = model.listIndividuals(resource);
			while (iteratorFood.hasNext()){
				Object o = iteratorFood.next();
				Individual individual = model.getIndividual(o.toString());
				String food = individual.getLocalName();
				//System.out.println("\t" + food);

				updateObject(categoryName, food);
			}
		}


		

		/*
		// Get all items
		ExtendedIterator it = model.listIndividuals();
		while (it.hasNext()){
			Object obj = it.next();
			Individual individual = model.getIndividual(obj.toString());
			//System.out.println(individual.getLocalName());
			//System.out.println("\t" + individual.getRDFType().getLocalName());

		}
		//Individual individual = model.getIndividual("http://www.semanticweb.org/yjj/ontologies/2017/5/untitled-ontology-10#Apple_Liquor");
		//System.out.println(individual.getLocalName());

		System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");*/


	}
	
	private void updateObject(String categoryName, String food){
		if (categoryName.equalsIgnoreCase("fruit")){
			Fruit.getInstance().addFruit(food);
		} else if (categoryName.equalsIgnoreCase("alcohol")){
			Alcohol.getInstance().addAlcohol(food);
		} else if (categoryName.equalsIgnoreCase("sweetener")){
			Sweetener.getInstance().addSweetener(food);
		} else if (categoryName.equalsIgnoreCase("enhancer")){
			Enhancer.getInstance().addEnhancer(food);
		} else if (categoryName.equalsIgnoreCase("garnish")){
			Garnish.getInstance().addGarnish(food);
		} else{
			System.out.println("Error!!! Can't find category " + categoryName);
		}
	}
}
