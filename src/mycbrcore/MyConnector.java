package mycbrcore;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import myerror.ErrorClass;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hp.hpl.jena.ontology.OntologyException;

import representation.CaseDescription;
import representation.Ingredient;

import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CaseBaseFilter;
import jcolibri.cbrcore.Connector;
import jcolibri.datatypes.Instance;
import jcolibri.exception.InitializingException;
import jcolibri.exception.OntologyAccessException;


public class MyConnector implements Connector {

	private Collection<CBRCase> cbrCollection;
	
	public MyConnector(String filename) {
		cbrCollection = new ArrayList<CBRCase>();
		
		try {
			File fXmlFile = new File(filename);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("recipe");
			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {			
				System.out.println("#" + (temp+1));
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					CBRCase cbrCase = new CBRCase();
					CaseDescription caseDescription = new CaseDescription();
					cbrCase.setDescription(caseDescription);
					caseDescription.setCaseId(String.valueOf(temp+1));
					
					

					Element eElement = (Element) nNode;
					
					// Step 1: title
					String title = eElement.getElementsByTagName("title").item(0).getTextContent();
					System.out.println("title: " + title);
					caseDescription.setTitle(title);
					
					
					// Step 2: ingredients <ingredient>
					String ingredients = eElement.getElementsByTagName("ingredients").item(0).getTextContent();
					//System.out.println("ingredients: " + ingredients);
		
					NodeList ingredientList = eElement.getElementsByTagName("ingredient");
					
					System.out.println("ingredients:");
					for (int i=0; i<ingredientList.getLength(); i++){
						Node aItem = ingredientList.item(i);
						NamedNodeMap namedNodeMap = aItem.getAttributes();
						
						// Step 2.1: Get quantity, unit, food
						double quantity = 0;
						String quantityTemp = namedNodeMap.getNamedItem("quantity").getNodeValue();
						if (quantityTemp!= null && !quantityTemp.equalsIgnoreCase("")){	 // because there are empty strings
							quantity = Double.valueOf(namedNodeMap.getNamedItem("quantity").getNodeValue());						
						}
						String unit = namedNodeMap.getNamedItem("unit").getNodeValue();
						String food = namedNodeMap.getNamedItem("food").getNodeValue();
						food = convertFood(food);
						String ingredientText = aItem.getTextContent();
						System.out.println("\t" + quantity + " " + unit + " " + food);
						
						
						try{
							Ingredient ingredient = new Ingredient(quantity, unit, new Instance(food));
							caseDescription.addIngredient(ingredient);
						} catch (OntologyAccessException e){
							//TESTING
							ErrorClass.getInstance().addError(e.getMessage());
							System.err.println(e.getMessage());
						}
					}

					
					
					
					
					

					// Step 3: preparation <step>
					//String preparation = eElement.getElementsByTagName("preparation").item(0).getTextContent();
					//System.out.println("Preparation: " + preparation);

					NodeList stepList = eElement.getElementsByTagName("step");
					String stepsCombine = "";
					for (int i=0; i<stepList.getLength(); i++){
						String aStep = stepList.item(i).getTextContent();
						stepsCombine = stepsCombine.concat(aStep).concat(" ");						
					}
					caseDescription.setSteps(stepsCombine);


					//System.out.println("title : " + title);
					//System.out.println("ingredients: " + ingredients);
					//System.out.println("stepsCombine: " + stepsCombine);
					System.out.println("---------------------------------");
					
					
					cbrCollection.add(cbrCase);
				}
				
				
				
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String convertFood(String food){
		food = food.toLowerCase().replaceAll(" ", "_");
		return food;
	}
	
	
	
	
	
	
	@Override
	public void close() {
		// Not implementing
	}

	@Override
	public void deleteCases(Collection<CBRCase> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void initFromXMLfile(URL arg0) throws InitializingException {
		// Not implementing
	}

	@Override
	public Collection<CBRCase> retrieveAllCases() {
		return cbrCollection;
	}

	@Override
	public Collection<CBRCase> retrieveSomeCases(CaseBaseFilter arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void storeCases(Collection<CBRCase> cbrCase) {
		// TODO Auto-generated method stub
	}

}
