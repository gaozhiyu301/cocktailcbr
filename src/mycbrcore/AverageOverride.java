package mycbrcore;

import java.util.ArrayList;

import representation.Ingredient;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.cbrcore.CaseComponent;
import jcolibri.method.retrieve.NNretrieval.NNConfig;
import jcolibri.method.retrieve.NNretrieval.similarity.GlobalSimilarityFunction;
import jcolibri.method.retrieve.NNretrieval.similarity.InContextLocalSimilarityFunction;
import jcolibri.method.retrieve.NNretrieval.similarity.LocalSimilarityFunction;
import jcolibri.method.retrieve.NNretrieval.similarity.StandardGlobalSimilarityFunction;
import jcolibri.util.AttributeUtils;

public class AverageOverride extends StandardGlobalSimilarityFunction {
	/**
	 * Average.java
	 * jCOLIBRI2 framework. 
	 * @author Juan A. Recio-Garc�a.
	 * GAIA - Group for Artificial Intelligence Applications
	 * http://gaia.fdi.ucm.es
	 * 03/01/2007
	 */



		public double computeSimilarity(double[] values, double[] weigths, int ivalue)
		{
			double acum = 0;
			double weigthsAcum = 0;
			for(int i=0; i<ivalue; i++)
			{
				acum += values[i] * weigths[i];
				weigthsAcum += weigths[i];
			}
			return acum/weigthsAcum;
		}
		
		
		public double compute(CaseComponent componentOfCase, CaseComponent componentOfQuery, CBRCase _case,
			    CBRQuery _query, NNConfig numSimConfig)
		    {
			GlobalSimilarityFunction gsf = null;
			LocalSimilarityFunction lsf = null;
			
			
			Attribute[] attributes = jcolibri.util.AttributeUtils.getAttributes(componentOfCase.getClass());
			
			double[] values = new double[attributes.length];
			double[] weights = new double[attributes.length];
			
			int ivalue = 0;

			for(int i=0; i<attributes.length; i++)
			{
				Attribute at1 = attributes[i];
				Attribute at2 = new Attribute(at1.getName(), componentOfQuery.getClass());
				
				try{
				if((gsf = numSimConfig.getGlobalSimilFunction(at1)) != null)
				{
					//modify list here
					
					values[ivalue] = gsf.compute((CaseComponent)at1.getValue(componentOfCase), (CaseComponent)at2.getValue(componentOfQuery), _case, _query, numSimConfig);
					weights[ivalue++] = numSimConfig.getWeight(at1); 
				}
				else if((lsf = numSimConfig.getLocalSimilFunction(at1))  != null)
				{
		       		    	if(lsf instanceof InContextLocalSimilarityFunction)
		       		    	{
		       		    	    InContextLocalSimilarityFunction iclsf = (InContextLocalSimilarityFunction)lsf;
		       		    	    iclsf.setContext(componentOfCase, componentOfQuery, _case, _query, at1.getName());
		       		    	}
       		    	if(at2.getValue(componentOfQuery) instanceof ArrayList){
       					for (Ingredient tmpIng : (ArrayList<Ingredient>)at2.getValue(componentOfQuery)){
				       		if(at1.getValue(componentOfCase) instanceof ArrayList){
				       			//ArrayList at1.getValue(componentOfCase);
				       			for (Ingredient tmp : (ArrayList<Ingredient>)at1.getValue(componentOfCase)){
		       				
		       						double value = lsf.compute(tmpIng.getFood(),tmp.getFood());
		       						if(values[ivalue]<value){
		       							values[ivalue]=value;
		       						}
		       					}
		       				}
		       			}
		       			//values[ivalue] = lsf.compute(at1.getValue(componentOfCase), at2.getValue(componentOfQuery));
		       		}
					weights[ivalue++] = 1.0/((ArrayList<Ingredient>)at2.getValue(componentOfQuery)).size();
				}
				}catch(Exception e)
				{
					System.err.println(e.getMessage());
					e.printStackTrace();
				}
			}
			
			return computeSimilarity(values, weights, ivalue);

		    }
}
