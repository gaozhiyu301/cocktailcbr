import java.util.ArrayList;
import java.util.Collection;

import jcolibri.casebase.LinealCaseBase;
import jcolibri.cbraplications.StandardCBRApplication;
import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRCaseBase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.cbrcore.Connector;
import jcolibri.exception.ExecutionException;
import jcolibri.method.retrieve.RetrievalResult;
import jcolibri.method.retrieve.NNretrieval.NNConfig;
import jcolibri.method.retrieve.NNretrieval.similarity.local.ontology.OntCosine;
import jcolibri.method.retrieve.selection.SelectCases;
import jcolibri.util.FileIO;
import mycbrcore.AverageOverride;
import mycbrcore.MyConnector;
import mycbrcore.MyOntology;
import mycbrcore.NNScoringMethodOverride;

import org.apache.log4j.Logger;

import representation.CaseDescription;
import representation.Ingredient;
import es.ucm.fdi.gaia.ontobridge.OntoBridge;
import es.ucm.fdi.gaia.ontobridge.OntologyDocument;




public class MyApplication implements StandardCBRApplication{

	private final static Logger logger = Logger.getLogger(MyApplication.class);

	private CBRCaseBase caseBase;
	private Connector connector;
	private Collection<CBRCase> cbrCollection;
	private NNConfig simConfig;
	
	@Override
	public void configure() throws ExecutionException {
		// Setting up ontology (includ. extracting items from ontology)
		MyOntology ontology = new MyOntology();	
		
		// Obtain a reference to OntoBridge
		OntoBridge ob = jcolibri.util.OntoBridgeSingleton.getOntoBridge();
		// Configure it to work with the Pellet reasoner
		ob.initWithPelletReasoner();
		// Setup the main ontology
		OntologyDocument mainOnto = new OntologyDocument("http://gaia.fdi.ucm.es/ontologies/ccc_cocktails.xml", 
								 FileIO.findFile("db/ccc_cocktails.xml").toExternalForm());
		// There are not subontologies
		ArrayList<OntologyDocument> subOntologies = new ArrayList<OntologyDocument>();
		// Load the ontology
		ob.loadOntology(mainOnto, subOntologies, false);
		
	
		// Now configure the KNN
		simConfig = new NNConfig();
		// Set the average() global similarity function for the description of the case
		simConfig.setDescriptionSimFunction(new AverageOverride());
		// HolidayType --> equal()
		Attribute IngredeintAttr = new Attribute("ingredients", CaseDescription.class);
		simConfig.addMapping(IngredeintAttr, new OntCosine());
	

		
	}



	@Override
	public CBRCaseBase preCycle() throws ExecutionException {
		caseBase = new LinealCaseBase();



		// Step 1: Setting up XML database
		MyConnector myConnector =  new MyConnector("db/ccc_cocktails.xml");
		cbrCollection = myConnector.retrieveAllCases();
//		System.out.println("Stored cases: " + cbrCollection.size());
//		for (CBRCase aCase : cbrCollection){
//			System.out.println(aCase);
//		}	


		return null;
	}


	@Override
	public void cycle(CBRQuery query) throws ExecutionException {	
		System.out.println("cycle-----");
		CaseDescription queryDescription = (CaseDescription)query.getDescription();
		System.out.println("query: " + queryDescription);
		
//		NNConfig simConfig = new NNConfig();
//		simConfig.setDescriptionSimFunction(new Average());
		
		/*Attribute foodAttr = new Attribute("food", Ingredient.class);
		simConfig.addMapping(foodAttr, new OntCosine());*/
		
//		Attribute testAttr = new Attribute("test", CaseDescription.class);
//		simConfig.addMapping(testAttr, new OntCosine());
		
		
		// Execute Nearest Neighbour Algo
		Collection<RetrievalResult> eval = NNScoringMethodOverride.evaluateSimilarity(cbrCollection, query, simConfig);
		
		// Select k cases
		eval = SelectCases.selectTopKRR(eval, 5);
		

		// print result
		for (RetrievalResult result : eval){
			System.out.println(result);
		}
	}

	@Override
	public void postCycle() throws ExecutionException {

	}



	public static void main(String[] args) {
		System.out.println("Program started...");

		MyApplication myApplication = new MyApplication();

		try {
			myApplication.configure();

			// test
			/*Fruit.getInstance().printAllFruits();
			Alcohol.getInstance().printAllAlcohols();
			Enhancer.getInstance().printAllEnhancers();
			Garnish.getInstance().printAllGarnishes();
			Sweetener.getInstance().printAllSweeteners();*/
			
			myApplication.preCycle();

			
			// TESTING to be deleted
			//ErrorClass.getInstance().printAllErrors();
			
			
			
			
			// TESTING
			CaseDescription caseDesc = new CaseDescription();
			caseDesc.addIngredient(new Ingredient(1, "cl", "tequila"));
			CBRQuery query = new CBRQuery();
			query.setDescription(caseDesc);
			myApplication.cycle(query);
			
		} catch (ExecutionException e) {
			e.printStackTrace();
		}


	}

}
