package myerror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;


public class ErrorClass {
	private static ErrorClass instance = null;
	private ArrayList<String> errors = new ArrayList<String>();
	
	private ErrorClass(){}
	
	public static ErrorClass getInstance(){
		if (instance == null){
			instance = new ErrorClass();
		}
		return instance;
	}
	
	public Collection<String> getErrors(){
		Collections.sort(errors);
		return errors;
	}
	
	public void addError(String error){
		if (!errors.contains(error)){
			errors.add(error);			
		}
	}
	
	public void printAllErrors(){
		Iterator<String> it = errors.iterator();
		int counter = 1;
		while (it.hasNext()){
			String error = it.next();
			System.err.println("---> #" + counter + " " + error);
			counter++;
		}
	}
	
}
